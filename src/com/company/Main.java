package com.company;

public class Main {

    public static void main(String[] args) {
        Book[] books = new Book[5];

        books[0] = new Book("Три мушкитера", "Александр Дюма", 1844);
        books[1] = new Book("Пеппи Длинныйчулок", "Астрид Линдгрен", 1945);
        books[2] = new Book("Три толстяка", "Юрий Олеша", 1924);
        books[3] = new Book("Граф Монте-Кристо", "Александр Дюма", 1845);
        books[4] = new Book("Вий", "Николай Гоголь", 1835);

        int oldYear = books[0].year;
        for (int i = 1; i < books.length; i++) {
            if (books[i].year < oldYear) {
                oldYear = books[i].year;
            }
        }

        for (Book book : books) {
            if (book.year == oldYear) {
                System.out.println(book.author);
            }
        }

        System.out.println("---------------------------------");

        String authorCheck = "Александр Дюма";
        for (Book book : books) {
            if (book.author.equals(authorCheck)) {
                System.out.println("Автор: " + book.author + " | Название: " + book.title);
                System.out.println();
            }
        }

        System.out.println("---------------------------------");

        int thisYear = 1900;
        for (Book book : books) {
            if (book.year < thisYear) {
                System.out.println("Автор: " + book.author + " | Название: " + book.title + " | Год издания: " + book.year);
            }
        }
    }
}
